﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class CometGenerator
    {
        public CometGenerator() { }

        public Comet Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            Comet comet = new Comet();

            comet.Diameter = random.NextDouble(0.15);
            comet.Mass = random.NextDouble() * comet.Diameter;
            comet.Density = random.NextDouble(1.5, 3.5);

            return comet;
        }
    }
}
